{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase                 #-}
{-# LANGUAGE NamedFieldPuns             #-}
{-# LANGUAGE PatternSynonyms            #-}
{-# LANGUAGE RebindableSyntax           #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeOperators              #-}

module FIR.Examples.RenderState
  ( Input(..), nullInput
  , Action(..)
  , Quit(Quit)
  , Observer(..), initialObserver
  , Observer2D(..), initialObserver2D, updateObserver2D
  , RenderState(..), initialState
  , _observer, _position, _angles, _input
  , mainLoop
  , onSDLInput
  , interpretInput
  , move
  , modelViewProjection
  , camera
  , pos2Coord
  ) where

-- base
import Prelude
  hiding ( Num(..), Fractional(..), Floating(..) )
import Data.Maybe
  ( fromMaybe )
import Data.Monoid
  ( Any(..), Sum(..) )
import GHC.TypeNats
  ( KnownNat )

-- lens
import Control.Lens
  ( Lens', lens )

-- sdl2
import qualified SDL

-- fir
import FIR
  hiding
    ( Input, Eq(..), Ord(..), Any
    , view
    , pure, (>>=), (>>)
    )
import Math.Linear
  ( V, M
  , pattern V2, pattern V3
  , norm, normalise
  , (^+^), (*^), (-^)
  , (!*!)
  , perspective, lookAt
  )
import Math.Quaternion
  ( Quaternion
  , rotate, axisAngle
  )

----------------------------------------------------------------------------

mainLoop :: Monad m => m Quit -> m ()
mainLoop mb
  = do
      b <- mb
      case b of
        Quit -> pure ()
        _    -> mainLoop mb

----------------------------------------------------------------------------

data RenderState
  = RenderState
    { observer    :: Observer
    , input       :: Input
    }

initialState :: RenderState
initialState =
  RenderState
    { observer = initialObserver
    , input    = nullInput
    }

_observer :: Lens' RenderState Observer
_observer = lens observer ( \s v -> s { observer = v } )

_position :: Lens' Observer (V 3 Float)
_position = lens position ( \s v -> s { position = v } )

_angles :: Lens' Observer (V 2 Float)
_angles = lens angles ( \s v -> s { angles = v } )

_input :: Lens' RenderState Input
_input = lens input ( \s v -> s { input = v } )


----------------------------------------------------------------------------

newtype Quit = MkQuit Any
  deriving (Eq, Show, Semigroup, Monoid)

pattern Quit :: Quit
pattern Quit = MkQuit (Any True)

data Input = Input
  { keysDown         :: [SDL.Scancode]
  , keysPressed      :: [SDL.Scancode]
  , mouseButtonsDown :: [SDL.MouseButton]
  , mousePos         :: V 2 Float
  , mouseRel         :: V 2 Float
  , mouseWheel       :: Int
  , quitEvent        :: Quit
  } deriving Show

data Action = Action
  { movement       :: V 3 (Sum Float)
  , look           :: V 2 (Sum Float)
  , shouldQuit     :: Quit
  , locate         :: Bool
  , takeScreenshot :: Bool
  } deriving Show

data Observer = Observer
  { position :: V 3 Float
  , angles   :: V 2 Float
  , clock    :: Float
  , frame    :: Word32
  } deriving Show

data Observer2D = Observer2D
  { zoom              :: Float
  , origin            :: V 2 Float
  , scroll            :: Int
  , mouseCoordPos     :: V 2 Float
  , mouseLeftDown     :: Bool
  , mouseLeftClicked  :: Bool
  , mouseRightDown    :: Bool
  , mouseRightClicked :: Bool
  } deriving Show

initialObserver2D :: Observer2D
initialObserver2D =
  let
    zoom = 1
    origin = V2 0 0
    scroll = 0
    mouseCoordPos = V2 0 0
    mouseLeftDown = False
    mouseLeftClicked = False
    mouseRightDown = False
    mouseRightClicked = False
  in Observer2D {..}

updateObserver2D :: Bool -> Observer2D -> V 2 Word32 -> Input -> Observer2D
updateObserver2D inversed Observer2D {..} screen Input {..} =
  let
    scrolled = mouseWheel - scroll
    newZoom =
      if scrolled /= 0
        then
          let
            offset = if scrolled < 0 then 0.1 else -0.1
           in zoom + offset * zoom
        else
         zoom

    inverse :: V 2 a -> V 2 a
    inverse (V2 x y)
      | inversed  = V2 y x
      | otherwise = V2 x y

    newMouseCoordPos = pos2Coord (inverse screen) origin zoom (inverse mousePos)

    newOrigin =
      if newMouseLeftClicked
        then mouseCoordPos
        else origin

    newScroll = mouseWheel
    newMouseRightDown = SDL.ButtonRight `elem` mouseButtonsDown
    newMouseLeftDown = SDL.ButtonLeft `elem` mouseButtonsDown
    newMouseLeftClicked =
      if newMouseLeftDown
        -- Clicked is True only the first time the event is received
        then Prelude.not mouseLeftDown
        else False
    newMouseRightClicked =
      if newMouseRightDown
        -- Clicked is True only the first time the event is received
        then Prelude.not mouseRightDown
        else False
   in
    Observer2D
      newZoom newOrigin newScroll newMouseCoordPos
      newMouseLeftDown newMouseLeftClicked newMouseRightDown newMouseRightClicked

nullInput :: Input
nullInput
  = Input
    { keysDown         = []
    , keysPressed      = []
    , mouseButtonsDown = []
    , mousePos         = V2 0 0
    , mouseRel         = V2 0 0
    , mouseWheel       = 0
    , quitEvent        = coerce False
    }

initialObserver :: Observer
initialObserver
  = Observer
      { position = V3 0 0 (-6)
      , angles   = pure 0
      , clock    = 0
      , frame    = 0
      }

p, n :: Float
p =  1
n = -1

strafeDir :: SDL.Scancode -> V 3 Float
strafeDir SDL.ScancodeW      = V3 0 0 p
strafeDir SDL.ScancodeS      = V3 0 0 n
strafeDir SDL.ScancodeA      = V3 n 0 0
strafeDir SDL.ScancodeD      = V3 p 0 0
strafeDir SDL.ScancodeLCtrl  = V3 0 p 0
strafeDir SDL.ScancodeLShift = V3 0 n 0
strafeDir SDL.ScancodeUp     = V3 0 0 p
strafeDir SDL.ScancodeDown   = V3 0 0 n
strafeDir SDL.ScancodeLeft   = V3 n 0 0
strafeDir SDL.ScancodeRight  = V3 p 0 0
strafeDir SDL.ScancodeRCtrl  = V3 0 p 0
strafeDir SDL.ScancodeRShift = V3 0 n 0
strafeDir _                  = V3 0 0 0

normaliseStrafing
  :: (KnownNat n, Floating a, DivisionRing a, Ord a)
  => V n a -> V n a
normaliseStrafing v
  | norm v < 0.01 = pure 0
  | otherwise     = normalise v


strafe :: Float -> [SDL.Scancode] -> V 3 (Sum Float)
strafe multiplier
  = coerce
  . ( multiplier *^ )
  . normaliseStrafing @3 @Float
  . coerce
  . foldMap (fmap Sum . strafeDir)


onSDLInput :: SDL.Window -> Input -> SDL.EventPayload -> Input
onSDLInput _ input SDL.QuitEvent
  = input { quitEvent = Quit }
onSDLInput _ input (SDL.WindowClosedEvent _)
  = input { quitEvent = Quit }
onSDLInput win input (SDL.KeyboardEvent ev)
  | SDL.keyboardEventWindow ev /= Just win = input
  | otherwise
  = let keyCode = SDL.keysymScancode (SDL.keyboardEventKeysym ev)
    in case SDL.keyboardEventKeyMotion ev of
         SDL.Pressed  -> input { keysDown    = keyCode : filter (/= keyCode) (keysDown    input)
                               , keysPressed = keyCode : filter (/= keyCode) (keysPressed input)
                               }
         SDL.Released -> input { keysDown = filter (/= keyCode) (keysDown input) }
onSDLInput win input (SDL.MouseButtonEvent ev)
  | SDL.mouseButtonEventWindow ev /= Just win = input
  | otherwise
  = let button = SDL.mouseButtonEventButton ev
        down = filter (/= button) (mouseButtonsDown input)
    in case SDL.mouseButtonEventMotion ev of
      SDL.Pressed  -> input { mouseButtonsDown = button : down }
      SDL.Released -> input { mouseButtonsDown = down }
onSDLInput win input (SDL.MouseMotionEvent ev)
  | SDL.mouseMotionEventWindow ev /= Just win = input
  | otherwise
  = input { mousePos = fmap Prelude.fromIntegral (V2 px py)
          , mouseRel = fmap ((* 0.003) . Prelude.fromIntegral) (V2 rx ry)
          }
    where
      SDL.P (SDL.V2 px py) = SDL.mouseMotionEventPos       ev
      SDL.V2        rx ry  = SDL.mouseMotionEventRelMotion ev
onSDLInput win input (SDL.MouseWheelEvent ev)
  | SDL.mouseWheelEventWindow ev /= Just win = input
  | otherwise
  = input { mouseWheel = mouseWheel input + Prelude.fromIntegral wheelYOffset }
    where
      SDL.V2 _ wheelYOffset = SDL.mouseWheelEventPos ev
onSDLInput _ input _ = input


interpretInput :: Float -> Input -> Action
interpretInput mul Input { .. } =
  let movement       = strafe mul keysDown
      escape         = foldMap
                          ( \case { SDL.ScancodeEscape -> Quit; _ -> mempty } )
                          keysPressed
      shouldQuit     = quitEvent <> escape
      look           = fmap Sum . (-^) $ mouseRel
      locate         = SDL.ScancodeSpace `elem` keysDown
      takeScreenshot = SDL.ScancodeF12   `elem` keysPressed
  in Action { .. }

move :: Observer -> Action -> (Observer, Quaternion Float)
move  Observer { position = oldPosition, angles = oldAngles, clock = oldClock, frame = oldFrame }
      Action   { .. }
  = let angles@(V2 x y) = oldAngles ^+^ fmap getSum look
        orientation = axisAngle (V3 0 (-1) 0) x * axisAngle (V3 1 0 0) y
        position = oldPosition ^+^ rotate orientation (fmap getSum movement)
        clock = oldClock
        frame = oldFrame + 1
    in ( Observer { .. }, orientation )

modelViewProjection :: Observer -> Maybe (Quaternion Float) -> M 4 4 Float
modelViewProjection Observer { angles = V2 x y, position } mbOrientation
  = let orientation
          = fromMaybe
              ( axisAngle (V3 0 (-1) 0) x * axisAngle (V3 1 0 0) y )
              mbOrientation
        forward = rotate orientation ( V3 0   0  1 ) -- Vulkan coordinate system
        up      = rotate orientation ( V3 0 (-1) 0 )
        view    = lookAt ( position ^+^ forward ) position up
        projection = perspective ( pi / 2 ) ( 16 / 9 ) 0.1 100000

    in projection !*! view

camera :: Observer
       -> Maybe (Quaternion Float)
       -> Struct
            '[ "position" ':-> V 3 Float
             , "right"    ':-> V 3 Float
             , "up"       ':-> V 3 Float
             , "forward"  ':-> V 3 Float
             ]
camera Observer { angles = V2 x y, position } mbOrientation
  = let
      orientation
        = fromMaybe
            ( axisAngle (V3 0 (-1) 0) x * axisAngle (V3 1 0 0) y )
            mbOrientation
      forward = rotate orientation ( V3 0   0  1 ) -- Vulkan coordinate system
      up      = rotate orientation ( V3 0 (-1) 0 )
      right   = rotate orientation ( V3 1   0  0 )
    in position :& right :& up :& forward :& End

{-| 'pos2Coord' convert a mouse position to a plane coordinate.

Screen is defined in pixels.
The plane is defined by the center coordinate (the @x@ mark) and
a range: the width of the screen in the plane (the dotted line).
The mouse position is defined in pixels (the @o@ mark).

 0 _________  screenX
  | o       |
  |....x....|
  |         |
  |_________|
 /
 screenY

Thus for a screen (800, 600), with a center at (0, 0) of range (1):
>>> let screen0 = pos2Coord (V2 800 800) (V2 0 0) 1
>>> screen0 (V2 0 0)
V2 -0.5 0.5
>>> screen0 (V2 800 800)
V2 0.5 -0.5
>>> screen0 (V2 400 400)
V2 0.0 -0.0
>>> screen0 (V2 0 800)
V2 -0.5 -0.5

The ratio is preserved for range
>>> let screen1 = pos2Coord (V2 800 600) (V2 0 0) 1
>>> screen1 (V2 800 600)
V2 0.6666667 -0.5
-}

pos2Coord :: V 2 Word32 -> V 2 Float -> Float -> V 2 Float -> V 2 Float
pos2Coord (V2 screenX' screenY') (V2 centerX centerY) range (V2 posX posY) =
  let
    (screenX, screenY) = (Prelude.fromIntegral screenX', Prelude.fromIntegral screenY')
    -- normalize mouse coordinate in a [0..1] range
    (uvX, uvY) = (posX / screenX, posY / screenY)
    -- move to plane coordinate and adjust for screen ratio
    coordX = (screenX / screenY) * (uvX - 0.5)
    coordY = (-1) * (uvY - 0.5)
    -- adjust for center and range
    x = centerX + coordX * range
    y = centerY + coordY * range
   in V2 x y
